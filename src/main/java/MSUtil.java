import net.lingala.zip4j.ZipFile;
import org.apache.commons.cli.*;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.tools.imageio.ImageIOUtil;

import javax.swing.JProgressBar;
import javax.swing.JFrame;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;


public class MSUtil extends JFrame {
    public static String keyword = "_assignsubmission_file_"; // true filename will come after that

    public static String unzipDestinationDirectory = "submissions"; //
    public static String topSheetDirectory = "topsheets";

    public static String errTag = "[ERR] ";

    public static boolean useCli = false;

    JProgressBar jProgressBar;

    public MSUtil() {
        setVisible(true);
        setTitle("Moodle Submission Utility");
        setSize(450, 150);
        setLayout(null);

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
    }

    public void setupProgressBar(int length) {
        jProgressBar = new JProgressBar(0, length);
        jProgressBar.setBounds(40, 40, 350, 30);
        jProgressBar.setValue(0);
        jProgressBar.setStringPainted(true);
        jProgressBar.setString("Files Renamed. Extracting topsheets...");
        add(jProgressBar);
    }

    public void incrementProgressBar() {
        jProgressBar.setValue(jProgressBar.getValue() + 1);
    }

    public static void setSoutToFile() {
        try {
            System.setOut(new PrintStream(new File("log.txt")));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static File[] getFilesWithExt(String directory, final String extension) {
        File dir = new File(directory);
        File [] files = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.endsWith(extension);
            }
        });
        return files;
    }

    public static File getSubmissionZipFile() throws Exception {
        File[] files = getFilesWithExt(".", ".zip");
        if(files.length > 1) {
            System.out.println(errTag + "Found multiple zip files. Please only keep the downloaded zip and run again.");
            throw new Exception("Multiple zip files");
        }
        if(files.length == 0) {
            System.out.println(errTag + "No zip file found. Please only keep the downloaded zip in this folder and run again.");
            throw new Exception("No zip file found");
        }
        File submissionZipFile = files[0];
        System.out.println("Found submission zip file: " + submissionZipFile.getName());
        return submissionZipFile;
    }

    public static void createUnzipDestinationDirectory() {
        File theDir = new File(unzipDestinationDirectory);
        if (!theDir.exists()){
            System.out.println("Unzip destination directory created named: " + unzipDestinationDirectory);
            theDir.mkdirs();
        }
        else {
            System.out.println("Clearing unzip destination directory: " + unzipDestinationDirectory);
            String[] entries = theDir.list();
            for(String s: entries){
                File currentFile = new File(theDir.getPath(), s);
                currentFile.delete();
            }
        }
    }

    public static void createFreshFolder(String name, String task) {
        File theDir = new File(name);
        if (!theDir.exists()){
            System.out.println(task + " directory created named: " + name);
            theDir.mkdirs();
        }
        else {
            System.out.println("Clearing " + task + " directory: " + name);
            String[] entries = theDir.list();
            for(String s: entries){
                File currentFile = new File(theDir.getPath(), s);
                currentFile.delete();
            }
        }
    }

    public static void unzipSubmissionZipFile(File submissionZipFile) throws IOException {
        System.out.println("Unzipping file " + submissionZipFile.getName());
        new ZipFile(submissionZipFile).extractAll(unzipDestinationDirectory);
        System.out.println("Unzipped");
    }

    public static void reportExtensions() throws Exception {
        File[] files = getFilesWithExt(unzipDestinationDirectory, "");

        HashMap<String, Integer> extensionCount = new HashMap<String, Integer>();

        ArrayList<String> nonPdfFiles = new ArrayList<String>();

        for(File file: files) {
            String extension = null;
            if(file.getName().lastIndexOf(".") == -1) {
                System.out.println(errTag + unzipDestinationDirectory + '/' + file.getName() + " has no extension");
            }
            else {
                extension = file.getName().substring(file.getName().lastIndexOf(".")).toUpperCase().substring(1);
            }

            if(extension == null || !extension.equals("PDF")) {
                nonPdfFiles.add(getTrueSubmissionFileName(file.getName()));
            }

            if(extensionCount.containsKey(extension)) {
                extensionCount.put(extension, extensionCount.get(extension) + 1);
            }
            else {
                extensionCount.put(extension, 1);
            }
        }

        System.out.println("---------- Count ----------");
        System.out.println("Total \t| " + files.length);
        for (HashMap.Entry<String, Integer> set : extensionCount.entrySet()) {
            System.out.println(set.getKey() + " \t| " + set.getValue());
        }
        System.out.println("---------------------------");

        System.out.println("--- Non Pdf Submissions ---");
        for(String str: nonPdfFiles) {
            System.out.println(str);
        }
        System.out.println("---------------------------");
    }

    public static String getTrueSubmissionFileName(String fileName) throws Exception {
        int i = fileName.lastIndexOf(keyword);
        if(i == -1) {
            System.out.println(errTag + "keyword " + keyword + " not found");
            System.out.println(errTag + "Failed to extract true file name of file: " + unzipDestinationDirectory + '/' + fileName);
            throw new Exception("No Keyword");
        }
        int j = i + keyword.length();
        if(j >= fileName.length()) {
            System.out.println(errTag + "keyword length + found length exceed file name length");
            System.out.println(errTag + "Failed to extract true file name of file: " + unzipDestinationDirectory + '/' + fileName);
            throw new Exception("Start substring incorrect");
        }
        return fileName.substring(j);
    }

    public static void renameFiles() throws Exception {
        File[] files = getFilesWithExt(unzipDestinationDirectory, "");
        for(File file: files) {
            String trueFileName = getTrueSubmissionFileName(file.getName());
            File newFile = new File(unzipDestinationDirectory + '/' + trueFileName);
            file.renameTo(newFile);
        }
    }

    public static void extractTopSheet(File file) throws Exception {
        PDDocument document = PDDocument.load(file);
        PDFRenderer pdfRenderer = new PDFRenderer(document);
        if(document.getNumberOfPages() == 0) {
            System.out.println(errTag + file.getName() + " has zero pages.");
            throw new Exception("Zero Page PDF");
        }
        int dpi = 150;
        BufferedImage bim = pdfRenderer.renderImageWithDPI(0, dpi, ImageType.RGB);
        ImageIOUtil.writeImage(bim, topSheetDirectory + '/' + file.getName() + ".png", dpi);
        document.close();
    }

    public static void extractTopSheetsForAll(MSUtil msUtil){
        System.out.println("Extracting top sheets to folder: " + topSheetDirectory);
        File[] pdfFiles = getFilesWithExt(unzipDestinationDirectory, ".pdf");

        System.out.println("---- Top Sheet Extraction Log ----");
        if(!useCli && msUtil != null) {
            msUtil.setupProgressBar(pdfFiles.length);
        }
        for(File file: pdfFiles) {
            try {
                extractTopSheet(file);
            }
            catch (Exception e) {
                System.out.println(errTag + file.getName() + " : Possible Corruption");
            }
            if(!useCli && msUtil != null) {
                msUtil.incrementProgressBar();
            }
        }
        System.out.println("----------------------------------");
    }

    public static void extractTopSheetsForAll() throws Exception {
        System.out.println("Extracting top sheets to folder: " + topSheetDirectory);
        File[] pdfFiles = getFilesWithExt(unzipDestinationDirectory, ".pdf");
        for(File file: pdfFiles) {
            extractTopSheet(file);
        }
    }

    public static boolean cliMode(String[] args) {
        Options options = new Options();

        Option input = new Option("c", "cli", true, "turn on command line output");
        input.setRequired(false);
        options.addOption(input);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("Moodle Submission Utility", options);
            System.exit(1);
        }
        if (cmd.getOptionValue("cli") == null) {
            return false;
        }
        return cmd.getOptionValue("cli").equals("true");
    }

    public static void main(String[] args) throws Exception {
        // runtime options
        useCli = cliMode(args);

        // suppress warning
        java.util.logging.Logger.getLogger("org.apache.pdfbox").setLevel(Level.SEVERE);
        java.util.logging.Logger.getLogger("org.apache.fontbox").setLevel(Level.SEVERE);

        // gui start
        MSUtil msUtil = null;
        if(!useCli) {
            msUtil = new MSUtil();
        }

        // all output to log.txt
        if(!useCli) {
            setSoutToFile();
        }

        System.out.println("Started on " + (new Date()));

        // find submission zip file
        File submissionZipFile = getSubmissionZipFile();

        // create a destination folder for unzipping
        //createUnzipDestinationDirectory();
        createFreshFolder(unzipDestinationDirectory, "unzip destination");

        // extract submission zip file
        unzipSubmissionZipFile(submissionZipFile);

        // report how files found, per extensions
        reportExtensions();

        // rename files
        renameFiles();

        // create a folder for showing top sheet
        createFreshFolder(topSheetDirectory, "top sheets");

        // extract top sheet and save with existing filename + number of pages
        extractTopSheetsForAll(msUtil);

        System.out.println("Finished on " + (new Date()));

        System.exit(0);
    }
}
